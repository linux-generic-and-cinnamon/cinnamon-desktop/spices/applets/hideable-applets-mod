# Changelog

### Version 2.0.0 MOD
Added alternative/additional way of marking hidden applets: grey background  
Added hover feedback for icons  

### Version 1.1.1
Fixed an issue of showing applets when panel edit mode is disabled in Cinnamon 3.8  
Fixed an issue of showing applets when a new applet is added to a panel in Cinnamon 3.8

### Version 1.1.0
Added search for applet icons in more directories  
Added an option to restore an applet as hidden if the applet shows up  
Added an option to show icons of hidden applets in grayscale  
Added an option to set a symbolic icon  
Added an option to match a size of applet icons to a size of a panel  
Fixed an issue where systray@cinnamon applet was registering clicks when hidden  

### Version 1.0.0
Initial release
